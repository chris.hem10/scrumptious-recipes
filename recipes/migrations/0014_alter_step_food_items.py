# Generated by Django 4.0.3 on 2022-07-15 20:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0013_alter_ingredient_amount_alter_ingredient_food_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='step',
            name='food_items',
            field=models.ManyToManyField(blank=True, to='recipes.fooditem'),
        ),
    ]
